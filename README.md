# Fall 2016 Simulated Emergency Test

This repository contains the sources for the documents supporting
the fall 2016 ARRL Simulated Emergency Test.

This project's
[wiki](https://gitlab.com/miarpsc-exercises/Ex16-2/wikis/home) is used
to capture input leading to the design of the exercise, and later, for
after-action comments.
