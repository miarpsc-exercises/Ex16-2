#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FILENAME "MSEL-list.csv"

char szBuffer[1024];
char szDate[32];
char szPara[1024];

int main( void )
{
    FILE *f;

    f = fopen(FILENAME,"r");
    if ( f==NULL )
	{
	    perror("Opening");
	    return 0;
	}
    while ( !feof(f) )
	{
	    fgets(szBuffer,sizeof(szBuffer),f);
	    if ( !feof(f) )
		{
		    memset(szDate,0,sizeof(szDate));
		    memset(szPara,0,sizeof(szPara));
		    memcpy(szDate,&szBuffer[1],22);
		    strcpy(szPara,&szBuffer[25]);
		    printf("  <formalpara>\n");
		    printf("    <title>%s</title>\n",szDate);
		    printf("    <para>\n");
		    printf("      %s",szPara);
		    printf("    </para>\n");
		    printf("  </formalpara>\n");
		}
	}
    
    return 1;
}
